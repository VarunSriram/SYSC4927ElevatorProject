#!/bin/bash

echo 'sep=,' > UniquelyAverageWaitTime.csv
echo 'sep=,' > UniquelyTotalWaitTime.csv
echo 'sep=,' > UniquelyTotalThroughputTime.csv

sort -un AverageWaitTime.csv >> UniquelyAverageWaitTime.csv
sort -un TotalWaitTime.csv >> UniquelyTotalWaitTime.csv
sort -un TotalThroughputTime.csv >> UniquelyTotalThroughputTime.csv 

start excel CreateGraphs.xlsm