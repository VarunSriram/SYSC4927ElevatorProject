package elevatorControlSystem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

import elevator.Elevator;
import patron.Patron;

public class Building extends Observable {
	
	/**
	 * A static Building instance that allows only one instance to be initialized
	 * 
	 */
	
	private static Building instance;
	
	
	/**
	 * Integer value representing the number of floors present in the building 
	 * 
	 */
	
	private int numOfFloors;
	
	
	/**
	 * Float value represent distance between each floor
	 * 
	 */
	
	private float floorDistance;
	
	
	/**
	 * A hashmap representing the floor number as the key and the List of Patrons on that particular floor
	 * 
	 */
	
	private HashMap<Integer, ArrayList<Patron>> patronLogger;
	
	
	/**
	 * A List of elevators present in the building
	 *  
	 */
	
	private ArrayList<Elevator> elevators;
	
	
	/**
	 * Dispatcher instance that dispatches the elevators according to the dispatching algorithms
	 * 
	 */
	
	private Dispatcher dispatcher;
	
	
	/**
	 * Getter for the instance of the Building
	 * 
	 * @return Building instance that allows only one instance to be initialized
	 * 
	 */
	
	public static Building getInstance(){
		
		if (instance == null)
			instance = new Building();
		
		return instance;
	}
	
	
	/**
	 * Constructor for the Building class
	 * 
	 */
	
	private Building (){
		
	}
	
	
	/**
	 * Sets up the Building given a particular dispatcher strategy
	 * 
	 * @param numberOfFloors integer value representing the number of floors present in the building
	 * @param floorDistance float value representing the distance between each floor
	 * @param elevators List of elevators present in the building
	 * @param pollTime float value representing polling frequency of the dispatcher
	 * @param strategy Dispatcher Strategy object representing the dispatching elevator strategy for 
	 * the dispatcher to use
	 * 
	 */
	
	public void setup(int numberOfFloors, float floorDistance, ArrayList<Elevator> elevators, float pollTime, DispatcherStrategy strategy){
		this.numOfFloors = numberOfFloors;
		this.floorDistance = floorDistance;
		this.patronLogger = new HashMap<Integer, ArrayList<Patron>>();
		this.elevators = elevators;
		
		for (int i = 0; i < this.numOfFloors; i++)
			this.patronLogger.put(i, new ArrayList<Patron>());
		
		this.dispatcher = new Dispatcher(numberOfFloors, pollTime, strategy);
		
	}
	
	
	/**
	 * Sets up the Building given a List of dispatcher strategies
	 * 
	 * @param numberOfFloors integer value representing the number of floors present in the building
	 * @param floorDistance float value representing the distance between each floor
	 * @param elevators List of elevators present in the building
	 * @param pollTime float value representing polling frequency of the dispatcher
	 * @param strategies List of Dispatcher Strategy object representing the dispatching elevator strategies 
	 * for the dispatcher to use
	 * 
	 */
	
	public void setup(int numberOfFloors, float floorDistance, ArrayList<Elevator> elevators, float pollTime, ArrayList<DispatcherStrategy> strategies){
		this.numOfFloors = numberOfFloors;
		this.floorDistance = floorDistance;
		this.patronLogger = new HashMap<Integer, ArrayList<Patron>>();
		this.elevators = elevators;
		
		for (int i = 0; i < this.numOfFloors; i++)
			this.patronLogger.put(i, new ArrayList<Patron>());
		
		this.dispatcher = new Dispatcher(numberOfFloors, pollTime, strategies);
		
	}
	
	
	/**
	 * Adds a Patron object to a particular floor
	 * 
	 * @param patron Patron object to be added to a particular floor
	 * @param floor integer value representing the floor number
	 * 
	 */
	
	public void addPatron(Patron patron, int floor){
		this.patronLogger.get(floor).add(patron);
		setChanged();
		this.notifyObservers(floor);
	}
	
	
	/**
	 * Getter for all the Patrons objects at a particular floor
	 * 
	 * @param floor integer value representing the floor number of the building
	 * @return List of Patrons at that particular floor
	 * 
	 */
	
	public ArrayList<Patron> getPatronAtFloor(int floor){
		return this.patronLogger.get(floor);
	}
	
	
	/**
	 * Removes a Patron given a floor from the list of Patrons on that floor
	 * 
	 * @param patron Patron object represent the Patron to be removed from the floor
	 * @param floor integer value representing the floor number of the building
	 * 
	 */
	
	public void removePatron(Patron patron, int floor){
		this.patronLogger.get(floor).remove(patron);
		setChanged();
		this.notifyObservers(floor);
	}
	
	
	/**
	 * Getter for the Elevators in the building
	 * 
	 * @return List of Elevators in the building
	 * 
	 */
	
	public ArrayList<Elevator> getElevator(){
		return this.elevators;
	}
	
	
	/**
	 * Adds an Elevator to the List of Elevators in the building
	 * 
	 * @param elevator Elevator object to be added to the List of Elevators in the building
	 * 
	 */
	
	public void addElevator(Elevator elevator){
		
		this.elevators.add(elevator);
	}
	
	
	/**
	 * Removes an Elevator from the List of Elevators in the building
	 * 
	 * @param elevator Elevator object to be removed from the List of Elevators in the building
	 * 
	 */
	
	public void removeElevator(Elevator elevator){
		this.elevators.remove(elevator);
	}
	
	
	/**
	 * Getter for the number of floors in the building
	 * 
	 * @return integer value representing the number of floors in the building
	 * 
	 */
	
	public int getNumberFloors(){
		return this.numOfFloors;
	}
	
	
	/**
	 * Starts each Elevator in the List
	 *  
	 */
	
	public void runElevators(){
		
		for(Elevator e: this.elevators){
			Thread thread = new Thread(e);
			thread.start();
		}	
	}
	
	
	/**
	 * Getter for the Dispatcher object
	 * 
	 * @return Dispatcher object representing the elevator dispatcher
	 * 
	 */
	
	public Dispatcher getDispatcher(){
		return this.dispatcher;
	}
	
	
	/**
	 * Runs the dispatcher for the elevator system
	 * 
	 */
	
	public void runDispatcher(){
		this.dispatcher.dispatch();
		//this.dispatcher.activateElevators();
	}
	
	
	/**
	 * Clears the Building of any Elevators and Patrons
	 * 
	 */
	
	public void clear(){
		
		Monitor.getInstance().deleteFiles();
		
		WriteToFile.deleteFiles("LogElevator.txt");
		WriteToFile.deleteFiles("LogPatron.txt");
	}
	
	
	/**
	 * Getter for the distance between each floor
	 * 
	 * @return float value representing the distance between each floor
	 * 
	 */
	
	public float getFloorDistance(){
		return this.floorDistance;
	}

}
