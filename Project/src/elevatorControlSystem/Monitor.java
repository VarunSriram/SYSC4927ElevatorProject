package elevatorControlSystem;
import java.util.ArrayList;

public class Monitor {
	
	/**
	 * integer constants representing the number of milliseconds in one second, and 
	 * the default initial value of the system
	 * 
	 */
	
	private static final int MILLISECONDS = 1000,
			                 DEFAULTINITIALVALUE = 0;
			                 
	
	
	/**
	 * String constants representing the names of the csv files to be created for dispatching 
	 * performance outputs
	 * 
	 */
	
	private static final String TOTALWAITTIMEFILE = "TotalWaitTime.csv",
								AVERAGEWAITTIMEFILE = "AverageWaitTime.csv",
								TOTALTHROUGHPUTTIMEFILE = "TotalThroughputTime.csv";
	
	
	/**
	 * A static Monitor instance that allows only one instance to be initialized
	 * 
	 */
	
	private static Monitor instance;
	
	
	/** 
	 * integer values representing the number of Patrons being serviced
	 * and the number of Patrons waiting
	 * 
	 */ 
	
	private int numberOfPatronsServiced,
				numberOfPatronsWaiting;
	
	
	/**
	 * float representing the the frequency of the Monitor 
	 * polling for new data
	 * 
	 */
	
	private float pollTime;
	
	
	/**
	 * long integer representing the current system time, total 
	 * accumulated wait time of the system, and total accumulated
	 * throughputTime
	 * 
	 */
	
	private long currentTime,
				 totalAccumulatedWaitTime,
				 totalAccumulatedThroughputTime;

	
	/**
	 * A List of Floats representing the wait and throughput times of each Patron
	 * 
	 */
	
	private ArrayList<Long> waitTimes,
							 throughputTimes;
	
	
	/**
	 * Constructor for the Monitor class
	 * 
	 */
	
	private Monitor(){
		waitTimes = new ArrayList<Long>();
		throughputTimes = new ArrayList<Long>();
	}
	
	
	/**
	 * Set ups the Monitor class
	 * 
	 * @param pollTime float value representing polling frequency of the dispatcher
	 * 
	 */

	public void setup(float pollTime) {
		this.currentTime = System.currentTimeMillis();
		this.numberOfPatronsServiced = DEFAULTINITIALVALUE;
		this.numberOfPatronsWaiting = DEFAULTINITIALVALUE;
		this.totalAccumulatedWaitTime = DEFAULTINITIALVALUE;
		this.totalAccumulatedThroughputTime = DEFAULTINITIALVALUE;
		this.pollTime = pollTime;
	}
	
	
	/**
	 * Getter for the instance of the Monitor
	 * 
	 * @return Monitor instance that allows only one instance to be initialized
	 * 
	 */
	
	public static Monitor getInstance(){
		if (instance == null)
			instance = new Monitor();
		
		return instance;
	}
	
	
	/**
	 * Getter for the number of Patrons being serviced
	 * 
	 * @return integer representing the number of Patrons being serviced
	 * 
	 */
	
	public int getNumberOfPatronsServiced() {
		return this.numberOfPatronsServiced;
	}
	

	/**
	 * Setter for the number of Patrons being serviced
	 * 
	 * @param numberOfPatronsServiced integer representing the number of Patrons
	 * being serviced
	 * 
	 */

	public void setNumberOfPatronsServiced(int numberOfPatronsServiced) {
		this.numberOfPatronsServiced = numberOfPatronsServiced;
	}


	/**
	 * Getter for number of Patrons waiting for the elevators
	 * 
	 * @return integer representing the number of Patrons waiting for the
	 * elevators
	 * 
	 */
	
	public int getNumberOfPatronsWaiting() {
		return this.numberOfPatronsWaiting;
	}


	/**
	 * Setter for the number of Patrons waiting for the elevators
	 * 
	 * @param numberOfPatronsWaiting integer representing the number of patrons waiting
	 * for the elevators
	 * 
	 */
	
	public void setNumberOfPatronsWaiting(int numberOfPatronsWaiting) {
		this.numberOfPatronsWaiting = numberOfPatronsWaiting;
	}

	
	/**
	 * Getter for the frequency of the Monitor polling for new data
	 * 
	 * @return float representing the frequency of the Monitor polling for new data
	 * 
	 */

	public float getPollTime() {
		return this.pollTime;
	}


	/**
	 * Setter for the frequency of the Monitor polling for new data
	 * 
	 * @param pollTime float representing the frequency of the Monitor polling 
	 * for new data
	 * 
	 */
	
	public void setPollTime(float pollTime) {
		this.pollTime = pollTime;
	}

	
	/**
	 * Getter for the current system time
	 * 
	 * @return float representing the current system time
	 * 
	 */

	public long getCurrentTime() {
		return System.currentTimeMillis();
	}


	/**
	 * Getter for the total accumulated wait time of the system
	 * 
	 * @return float representing the total accumulated wait time of
	 * system
	 * 
	 */
	
	public long getTotalAccumulatedWaitTime() {
		return this.totalAccumulatedWaitTime;
	}
	
	
	/**
	 * Adds the wait time of a particular Patron to the List of wait times
	 * 
	 * @param time float value representing the wait time of the Patron
	 * 
	 */
	
	public void addWaitTimeEntry(long time){
		this.waitTimes.add(time);
	}
	
	
	/**
	 * Adds the throughput time of a particular Patron to the List of throughput times
	 * 
	 * @param time float value representing the throughput time of the Patron
	 * 
	 */
	
	public void addThroughputTimeEntry(long time){
		this.throughputTimes.add(time);
	}
	
	
	/**
	 * Setter for the total accumulated wait time of the system
	 * 
	 */

	public void setTotalAccumulatedWaitTime() {
		this.totalAccumulatedWaitTime = DEFAULTINITIALVALUE;
		
		for (long wTime: this.waitTimes)
			this.totalAccumulatedWaitTime += (wTime / MILLISECONDS);
	}
	
	
	/**
	 * Setter for the total accumulated wait time of the system
	 * 
	 */

	public void setTotalAccumulatedThroughputTime() {
		this.totalAccumulatedThroughputTime = DEFAULTINITIALVALUE;
		
		for (long tpTime: this.throughputTimes)
			this.totalAccumulatedThroughputTime += (tpTime / MILLISECONDS);
	}
	
	
	
	/**
	 * Increments the number of Patrons being serviced
	 * 
	 */
	
	public void incrementPatronCount(){
		this.numberOfPatronsServiced ++;
	}
	
	
	/**
	 * Calculates the average wait time for the Patrons and saves it to the
	 * csv file
	 * 
	 */
	
	public void recordAverageWaitTime()	{
		
		if (this.waitTimes.size() != 0){
			this.setTotalAccumulatedWaitTime();
			
			float calc =  (this.totalAccumulatedWaitTime * 1.0f) / this.waitTimes.size();
			
			long timeDiff = Math.abs(System.currentTimeMillis() - this.currentTime) / MILLISECONDS;
			
			String data = "" + timeDiff + "," + calc + "\n";
			
			WriteToFile.csvAppend(AVERAGEWAITTIMEFILE, data);
		}
	}
	
	
	/**
	 * Calculates the throughput of the system and saves it to the
	 * csv file
	 * 
	 */
	
	public void recordThroughput() {
		
		if (this.throughputTimes.size() != 0) {
			this.setTotalAccumulatedThroughputTime();
			
			long timeDiff = Math.abs(this.currentTime - System.currentTimeMillis()) / MILLISECONDS;
			
			String data = "" + timeDiff  + "," + this.totalAccumulatedThroughputTime + "\n";
			
			WriteToFile.csvAppend(TOTALTHROUGHPUTTIMEFILE, data);
		}
		
	}
	
	
	/**
	 * Calculates the wait time of the system and saves it to the
	 * csv file
	 * 
	 */
	
	public void recordWaitTime(){
		
		if (this.waitTimes.size() != 0){
			this.setTotalAccumulatedWaitTime();
			
			long timeDiff = Math.abs(this.currentTime - System.currentTimeMillis()) / MILLISECONDS;
			
			String data = "" + timeDiff + "," + this.totalAccumulatedWaitTime + "\n";
			
			WriteToFile.csvAppend(TOTALWAITTIMEFILE, data);
		}
		
			
	}
	
	
	/**
	 * Calculates the performance of the system and saves it to the
	 * csv file
	 * 
	 */
	
	public void recordPerformanceMetrics() {
		
		if (Math.abs(this.currentTime - System.currentTimeMillis()) % this.pollTime == 0) {
			this.recordAverageWaitTime();
			this.recordThroughput();
			this.recordWaitTime();
		}
	}
	
	
	/**
	 * Deletes the files being created during the simulation
	 * 
	 */
	
	public void deleteFiles(){
		WriteToFile.deleteFiles(AVERAGEWAITTIMEFILE);
		WriteToFile.deleteFiles(TOTALTHROUGHPUTTIMEFILE);
		WriteToFile.deleteFiles(TOTALWAITTIMEFILE);
	}
	

}
