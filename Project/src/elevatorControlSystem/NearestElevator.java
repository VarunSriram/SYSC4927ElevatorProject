package elevatorControlSystem;
import java.util.ArrayList;

import elevator.Elevator;


public class NearestElevator implements DispatcherStrategy {

	@Override
	public Elevator pickElevator(ArrayList<Elevator> elevators,int requestFloor) {

		
		
		int score = 0, 
		    maxScore = 0,
		    elevCount = elevators.size(),
		    floorNum = Building.getInstance().getNumberFloors();
		    
		
		Elevator elevator = null;
		
		boolean allSameFloors = true;
		
		
		for (int i = 0; i < elevCount - 1; i++){
			if (elevators.get(i).getCurrentFloor() != elevators.get(i+1).getCurrentFloor())
				allSameFloors = false;
		}
		
		for (int i = 0; i < elevCount; i++) {
			
			if (allSameFloors){
				int sectorSize = floorNum / elevCount;
				
				for(int j = 1; j <= elevCount; j++){
					if(requestFloor <= j * sectorSize){
						return elevators.get(j-1);
					}
				}
			}
			
			if (!allSameFloors){
				if (elevators.get(i).getDirection() && elevators.get(i).getCurrentFloor() <= requestFloor)
					score = (floorNum + 1) - (requestFloor - elevators.get(i).getCurrentFloor());
				
				else if(!elevators.get(i).getDirection() && elevators.get(i).getCurrentFloor() >= requestFloor)
					score = floorNum + (elevators.get(i).getCurrentFloor() - requestFloor);
				
				if (score > maxScore) {
					maxScore = score;
					elevator = elevators.get(i);
				}
			}
			
		}
		
		
		return elevator;
	}

	@Override
	public void dispatchElevator(Elevator elevator, int destination) {
		elevator.addDestination(destination);
	}

}
