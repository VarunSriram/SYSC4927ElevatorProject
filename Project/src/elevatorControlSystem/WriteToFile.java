package elevatorControlSystem;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteToFile {

	/**
	 * Appends to end of a .csv file if it exists, otherwise it will create the file and then write to it
	 * 
	 * @param fileName String object representing the name of the file to write to including the extension of the file
	 * @param dataToWrite String object representing the data to be written into the file
	 * 
	 */
	
	public static void csvAppend(String fileName, String dataToWrite){
		
		try {
			File file = new File(fileName);
			
			
			if (!file.exists())
				file.createNewFile();
			
			BufferedWriter bufferWriter = new BufferedWriter(new FileWriter(fileName, true));
			bufferWriter.write(dataToWrite);
			bufferWriter.flush();
			bufferWriter.close();
			
		} catch (IOException e){
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * Deletes the file in the current directory
	 * 
	 * @param fileName String object representing the name of the file to be deleted if it exists
	 * 
	 */
	
	public static void deleteFiles(String fileName){
		
		try{
			File file = new File(fileName);
		
			file.delete();
			
		} catch (Exception e){
			System.out.println(fileName + " cannot be found");
		}
		
	}
}
