package elevatorControlSystem;
import java.util.ArrayList;

import elevator.Elevator;


public interface DispatcherStrategy {
	
	/**
	 * Picks the next available elevator from a list of elevators
	 * 
	 * @param elevators List of elevators in the building
	 * @return elevator representing the next available elevator
	 * 
	 */
	
	public Elevator pickElevator(ArrayList<Elevator> elevators , int requestFloor);
	
	
	/**
	 * Dispatch the particular elevator to a specific floor
	 * 
	 * @param elevator representing the next available elevator to be dispatched
	 * @param destination integer value represent the destination floor to which
	 * elevator is to be dispatched
	 * 
	 */
	
	public void dispatchElevator(Elevator elevator, int destination);
	
}
