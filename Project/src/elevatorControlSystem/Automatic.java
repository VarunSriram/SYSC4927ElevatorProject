package elevatorControlSystem;
import java.util.ArrayList;

import elevator.Elevator;


public class Automatic implements DispatcherStrategy {
	
	/**
	 * Dispatcher Strategy representing the strategy needed to dispatch the elevators
	 * 
	 */
	
	private DispatcherStrategy strategy;
	
	
	/**
	 * A list of dispatching strategies representing the various available strategies 
	 * needed to dispatch the elevators
	 * 
	 */
	
	private ArrayList<DispatcherStrategy> strategies;
	

	@Override
	public Elevator pickElevator(ArrayList<Elevator> elevators, int requestFloor) {
		this.pickStrategy(this.strategies);
		
		return this.strategy.pickElevator(elevators, requestFloor);
		
	}

	@Override
	public void dispatchElevator(Elevator elevator, int destination) {
		elevator.addDestination(destination);
	}
	
	
	/**
	 * Automatically chooses the best appropriate dispatching strategy from a list of provided strategies
	 * 
	 * @param strategies List of Dispatcher Strategies that the building owner has provided
	 * 
	 */
	
	public Automatic(ArrayList<DispatcherStrategy> strategies){
		this.strategies = strategies;
	}
	
	
	/**
	 * Automatically chooses the best appropriate dispatching strategy from a list of provided strategies
	 * 
	 * @param strategies List of Dispatcher Strategies that the building owner has provided
	 * 
	 */
	
	private void pickStrategy(ArrayList<DispatcherStrategy> strategies){
		if (Monitor.getInstance().getNumberOfPatronsWaiting() >= Monitor.getInstance().getNumberOfPatronsServiced())
			this.strategy = strategies.get(0);
		
		else
			this.strategy = strategies.get(1);
		
	}

}
