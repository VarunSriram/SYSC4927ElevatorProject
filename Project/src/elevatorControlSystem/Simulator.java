package elevatorControlSystem;
import java.util.ArrayList;
import java.util.Random;

import elevator.Elevator;
import guiMVC.ElevGrid;
import guiMVC.ElevatorTracker;
import guiMVC.GUIController;
import guiMVC.OperatorGUI;
import patron.Patron;


public class Simulator {
	
	/**
	 * boolean value representing whether the system is on or off
	 * 
	 */
	
	private boolean active;
	
	
	/**
	 * Building instance representing the Building that is being simulated
	 * 
	 */
	
	private Building building;
	
	
	/**
	 * List of Elevators present in the building that the Patrons can use
	 * 
	 */
	
	private ArrayList<Elevator> elevators;
	
	
	/**
	 * integer values representing the maximum number of patrons in the simulation
	 * and the number of floors in the building being simulated
	 * 
	 */
	
	private int maxPatrons,
				numOfFloors;
	
	
	/**
	 * List patrons representing a new batch of Patrons
	 * 
	 */
	
	private ArrayList<Patron> activePatron;
	
	
	/**
	 * GUI for the operator
	 * 
	 */
	
	private OperatorGUI opGUI;
	
	
	/**
	 * Creates a new batch of Patrons
	 * 
	 */
	
	public void createNewBatch() {
		
		int weight,
		    startFloor,
		    destinationFloor;
			
		Random randint = new Random();
		
		for (int i = 0; i < this.maxPatrons; i++) {
			weight = randint.nextInt(150);
			
			// makes sure that the weight in kg is greater than 20 for the Patron
			while(weight < 20)
				weight = randint.nextInt(150);
			
			destinationFloor = randint.nextInt(this.numOfFloors);
			startFloor = randint.nextInt(this.numOfFloors);
			
			// makes sure that the destination floor does not equal the start floor for the Patron
			while(destinationFloor == startFloor)
				destinationFloor = randint.nextInt(this.numOfFloors);
			
			this.activePatron.add(new Patron(destinationFloor, startFloor, weight, i));
		}
	}
	
	
	/**
	 * Runs the simulator
	 * 
	 */
	
	public void runSim() {
		this.active = true;
		this.building.runElevators();
		
		this.addPatronsToBuilding();
		
		//int count = 0;
		while (this.active) {
			
			this.opGUI.enableButton();
				
			
			this.building.runDispatcher();
			//count ++;
		}
		
		this.building.clear();	
	}
	
	
	/**
	 * Sets up the simulator with a particular dispatching strategy
	 * 
	 * @param maxPatron integer value representing the maximum number of Patrons in the simulation
	 * @param numberOfFloors integer value representing the number of floors in the building
	 * @param numberOfElevators integer value representing the number of elevators in the building
	 * @param floorDistance float value representing the distance between each floor
	 * @param pollTime float value representing the polling frequency of the Dispatcher
	 * @param speed float value representing the speed of the elevators
	 * @param doorSpeed integer value representing the speed of the door of the elevator
	 * @param capacity integer value representing the weight capacity of the elevator
	 * @param strategy Dispatching strategy available for the elevator dispatcher to use
	 * 
	 */
	
	public void setupSim(int maxPatron, int numberOfFloors, int numberOfElevators, float floorDistance, float pollTime, float speed, int doorSpeed, int capacity, DispatcherStrategy strategy){	
		this.active = false;
		this.activePatron = new ArrayList<Patron>();
		this.maxPatrons = maxPatron;
		this.numOfFloors = numberOfFloors;
		this.elevators = new ArrayList<Elevator>();
		
		for (int i = 0; i < numberOfElevators; i++){
			System.out.println("Adding elevator "+i);
			this.elevators.add(new Elevator(speed, doorSpeed, capacity, i));
		}
		this.building = Building.getInstance();
		this.building.setup(numberOfFloors, floorDistance, this.elevators, pollTime, strategy);
		this.building.clear();
		System.out.println("Floor dist" + floorDistance);
		ElevGrid gui = new ElevGrid(numberOfFloors,numberOfElevators);
		ElevatorTracker gui1 = new ElevatorTracker(numberOfElevators, gui.getX(), gui.getY(), gui.getWidth());
		GUIController ctl = new GUIController(gui,gui1);
		ctl.setupGUI();
		
		this.opGUI = new OperatorGUI();
		
	}
	
	
	/**
	 * Sets up the simulator 
	 * 
	 * @param maxPatron integer value representing the maximum number of Patrons in the simulation
	 * @param numberofFloors integer value representing the number of floors in the building
	 * @param numberOfElevators integer value representing the number of elevators in the building
	 * @param floorDistance float value representing the distance between each floor
	 * @param pollTime float value representing the polling frequency of the Dispatcher
	 * @param speed float value representing the speed of the elevators
	 * @param doorSpeed integer value representing the speed of the door of the elevator
	 * @param capacity integer value representing the weight capacity of the elevator
	 * @param strategies List of dispatching strategies available for the elevator dispatcher to use
	 * 
	 */
	
	public void setupSim(int maxPatron, int numberOfFloors, int numberOfElevators, float floorDistance, float pollTime, float speed, int doorSpeed, int capacity, ArrayList<DispatcherStrategy> strategies){	
		this.active = false;
		this.activePatron = new ArrayList<Patron>();
		this.maxPatrons = maxPatron;
		this.numOfFloors = numberOfFloors;
		
		this.elevators = new ArrayList<Elevator>();
		
		for (int i = 0; i < numberOfElevators; i++)
			this.elevators.add(new Elevator(speed, doorSpeed, capacity,i));
		
		this.building = Building.getInstance();
		this.building.setup(numberOfFloors, floorDistance, this.elevators, pollTime, strategies);
		this.building.clear();
		ElevGrid gui = new ElevGrid(numberOfFloors,numberOfElevators+1);
		ElevatorTracker gui1 = new ElevatorTracker(numberOfElevators, gui.getX(), gui.getY(), gui.getWidth());
		GUIController ctl = new GUIController(gui,gui1);
		ctl.setupGUI();
		
		this.opGUI = new OperatorGUI();
	}
	
	
	/**
	 * Adds a new Elevator to the simulation
	 * 
	 * @param speed float representing the speed of the elevator
	 * @param doorSpeed integer representing the speed of the elevator's door
	 * @param capactity integer representing the capacity of the elevator
	 * @param id integer representing the id number of the elevator
	 * 
	 */
	
	public void addNewElevator(float speed, int doorSpeed, int capactity, int id) {
		Elevator elevator = new Elevator(speed, doorSpeed, capactity, id);
		this.building.addElevator(elevator);
	}
	
	
	/**
	 * Removes an elevator given an elevator from the simulation
	 * 
	 * @param elevator instance to be removed from the simulation
	 * 
	 */
	
	public void removeElevator(Elevator elevator) {
		this.building.removeElevator(elevator);
	}
	
	
	/**
	 * Setter for the number of floors in the building being simulated
	 * 
	 * @param floors integer representing the number of floors in the building 
	 * being simulated
	 * 
	 */
	
	public void setFloors(int floors){
		this.numOfFloors = floors;
	}
	
	
	/**
	 * Resets the simulator to default values
	 * 
	 * @param maxPatron integer value representing the maximum number of Patrons in the simulation
	 * @param numberofFloors integer value representing the number of floors in the building
	 * @param numberOfElevators integer value representing the number of elevators in the building
	 * @param floorDistance float value representing the distance between each floor
	 * @param pollTime float value representing the polling frequency of the Dispatcher
	 * @param speed float value representing the speed of the elevators
	 * @param doorSpeed integer value representing the speed of the door of the elevator
	 * @param capacity integer value representing the weight capacity of the elevator
	 * @param strategies List of dispatching strategies available for the elevator dispatcher to use
	 * 
	 */
	
	public void reset(int maxPatron, int numberOfFloors, int numberOfElevators, float floorDistance, float pollTime, float speed, int doorSpeed, int capacity, ArrayList<DispatcherStrategy> strategies) {
		
		for(Patron patron: this.activePatron)
			patron.setEnded(true);
		
		this.building.clear();
		this.setupSim(maxPatron, numberOfFloors, numberOfElevators, floorDistance, pollTime, speed, doorSpeed, capacity, strategies);
	}
	
	
	/**
	 * Resets the simulator to default values
	 * 
	 * @param maxPatron integer value representing the maximum number of Patrons in the simulation
	 * @param numberofFloors integer value representing the number of floors in the building
	 * @param numberOfElevators integer value representing the number of elevators in the building
	 * @param floorDistance float value representing the distance between each floor
	 * @param pollTime float value representing the polling frequency of the Dispatcher
	 * @param speed float value representing the speed of the elevators
	 * @param doorSpeed integer value representing the speed of the door of the elevator
	 * @param capacity integer value representing the weight capacity of the elevator
	 * @param strategy Dispatching strategy available for the elevator dispatcher to use
	 * 
	 */
	
	public void reset(int maxPatron, int numberOfFloors, int numberOfElevators, float floorDistance, float pollTime, float speed, int doorSpeed, int capacity, DispatcherStrategy strategy) {
		
		for(Patron patron: this.activePatron)
			patron.setEnded(true);
		
		this.building.clear();
		this.setupSim(maxPatron, numberOfFloors, numberOfElevators, floorDistance, pollTime, speed, doorSpeed, capacity, strategy);
	}

	
	/**
	 * Getter for determining whether or not the simulation is active
	 * 
	 * @return boolean value representing whether or not the simulation is active
	 * 
	 */
	
	public boolean isActive() {
		return this.active;
	}

	
	/**
	 * Setter for determining whether or not the simulation is active
	 * 
	 * @param active boolean value representing whether or not the simulation is active
	 * 
	 */
	
	public void setActive(boolean active) {
		this.active = active;
	}

	
	/**
	 * Getter for the maximum number of Patrons 
	 * 
	 * @return integer the maximum number of Patrons 
	 * 
	 */
	
	public int getMaxPatrons() {
		return this.maxPatrons;
	}

	
	/**
	 * Setter the maximum number of Patrons in the simulation
	 * 
	 * @param maxPatrons integer value representing the maximum number of 
	 * Patrons in the simulation
	 * 
	 */
	
	public void setMaxPatrons(int maxPatrons) {
		this.maxPatrons = maxPatrons;
	}

	
	/**
	 * Getter for List of active Patrons
	 * 
	 * @return List of patrons representing a new batch of Patrons
	 * 
	 */
	
	public ArrayList<Patron> getActivePatron() {
		return this.activePatron;
	}

	
	/**
	 * Setter for List of active Patrons
	 * 
	 * @param activePatron List of patrons representing a new batch of Patrons
	 * 
	 */
	
	public void setActivePatron(ArrayList<Patron> activePatron) {
		this.activePatron = activePatron;
	}

	
	/**
	 * Getter for number of floors in the building
	 * 
	 * @return integer value representing the number of floors in the building
	 * 
	 */
	
	public int getNumOfFloors() {
		return this.numOfFloors;
	}
	
	
	/**
	 * Adds the newly created batch of Patrons to the building
	 * 
	 */
	
	public void addPatronsToBuilding(){
		
		this.createNewBatch();
		
		for(Patron patron: this.activePatron){
			this.building.addPatron(patron, patron.getStartFloor());
			
			Thread thread = new Thread(patron);
			thread.start();
		}
	}
	

}
