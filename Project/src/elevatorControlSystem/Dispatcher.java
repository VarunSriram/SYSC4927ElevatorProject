package elevatorControlSystem;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import elevator.Elevator;
import patron.Patron;


public class Dispatcher {
	 
	
	/**
	 * List of all the elevators in the building
	 * 
	 */
	
	private ArrayList<Elevator> elevators;
	
	
	/**
	 * Set of patrons requesting elevators in the building
	 * 
	 */
	
	private Set <Patron> requestingPatrons;
	
	
	/**
	 * integer value representing the number of floors in the building
	 * 
	 */
	
	private int numberOfFloors;
	
	
	/**
	 * DispatcherStrategy instance representing the dispatching elevator strategy
	 * 
	 */
	
	private DispatcherStrategy strategy;
	
	
	/**
	 * Monitor instance representing the monitor of the system
	 * 
	 */
	
	private Monitor monitor;
	
	private boolean once = true;
	
	
	/**
	 * Constructor for the Dispatcher class with specific strategy
	 * 
	 * @param floors integer representing the number of floors in the building
	 * @param pollTime float representing the polling frequency of the monitor
	 * @param strategy DispatcherStrategy representing a specific strategy for 
	 * dispatching elevators
	 * 
	 */
	
	public Dispatcher(int floors, float pollTime, DispatcherStrategy strategy) {
		this.monitor = Monitor.getInstance();
		this.monitor.setup(pollTime);
		this.numberOfFloors = floors;
		this.strategy = strategy;
		this.requestingPatrons = new HashSet<Patron>();
		this.elevators = Building.getInstance().getElevator();
	}
	
	
	/**
	 * Constructor for the Dispatcher class
	 * 
	 * @param floors integer representing the number of floors in the building
	 * @param pollTime float representing the polling frequency of the monitor
	 * @param strategies list representing the different types of dispatching strategies 
	 * available for the dispatcher
	 * 
	 */
	
	public Dispatcher(int floors, float pollTime, ArrayList<DispatcherStrategy> strategies) {
		this.monitor = Monitor.getInstance();
		this.monitor.setup(pollTime);
		this.numberOfFloors = floors;
		this.strategy = new Automatic(strategies);
		this.requestingPatrons = new HashSet<Patron>();
		this.elevators = Building.getInstance().getElevator();
	}


	/**
	 * Getter for list of all the elevators in the building
	 * 
	 * @return List representing all the elevators in the building
	 * 
	 */
	
	public ArrayList<Elevator> getElevators() {
		return this.elevators;
	}


	/**
	 * Setter for the list of all the elevators in the building
	 * 
	 */
	
	public void setElevators() {
		this.elevators = Building.getInstance().getElevator();
	}


	/**
	 * Getter for the set representing all the patrons requesting elevators in the building
	 * 
	 * @return Set representing all the patrons requesting elevators in the building
	 * 
	 */
	
	public Set<Patron> getRequestingPatrons() {
		return this.requestingPatrons;
	}
	
	
	/**
	 * Adds a requesting Patron to the Set of Patrons
	 * 
	 * @param patron Patron object representing the Patron requesting an elevator
	 * 
	 */
	
	public synchronized void addRequest(Patron patron){
		this.requestingPatrons.add(patron);
	}


	/**
	 * Getter for the number of floors in the building
	 * 
	 * @return integer representing the number of floors in the building
	 * 
	 */
	
	public int getNumberOfFloors() {
		return this.numberOfFloors;
	}


	/**
	 * Setter for the number of floors in the building
	 * 
	 * @param numberOfFloors integer representing the number of 
	 * floors in the building
	 * 
	 */
	
	public void setNumberOfFloors(int numberOfFloors) {
		this.numberOfFloors = numberOfFloors;
	}


	/**
	 * Getter for the elevator dispatching strategy
	 * 
	 * @return DispatcherStrategy representing the elevator dispatching strategy
	 * 
	 */
	
	public DispatcherStrategy getStrategy() {
		return this.strategy;
	}


	/**
	 * Setter for the elevator dispatching strategy
	 * 
	 * @param strategy DispatcherStrategy representing the dispatching elevator strategy
	 * 
	 */
	
	public void setStrategy(DispatcherStrategy strategy) {
		this.strategy = strategy;
	}
	
	
	/**
	 * Sets a particular elevator on hold
	 * 
	 * @param elevator Elevator instance representing the elevator that is on hold
	 * 
	 */
	
	public void activeHold(Elevator elevator){
		//TODO
	}
	
	
	/**
	 * Dispatches the next available elevator to a particular floor based on the dispatching
	 * strategy
	 * 
	 * @param floor integer representing the floor to which the elevator will be dispatched
	 * 
	 */
	
	private void dispatch(int floor) {
		Elevator e = this.strategy.pickElevator(elevators, floor);
		strategy.dispatchElevator(e, floor);
		
	}
	
	
	/**
	 * Dispatches the next available elevator to a particular floor based on the dispatching
	 * strategy
	 * 
	 */
	
	public void dispatch() {
		Patron patron = this.getRequest();
		
		this.popPatron();
		
		this.monitor.recordPerformanceMetrics();
		
		if (patron != null)
			this.dispatch(patron.getStartFloor());
		
	}
	
	
	/**
	 * Activates all the elevators to be on standby
	 * 
	 */
	
	public void activateElevators(){
		//TODO
		if (this.once) {
			this.dispatch(1);
			this.dispatch(2);
			this.dispatch(3);
			this.dispatch(5);
			this.dispatch(0);
			this.dispatch(4);
			
		}
		
		this.once = false;
	}
	
	
	/**
	 * Gets the first requesting Patron in the set of requesting Patrons
	 * 
	 * @return Patron instance representing the first requesting Patron 
	 * in the set of requesting Patrons
	 * 
	 */
	
	public synchronized Patron getRequest(){
		if (!this.requestingPatrons.isEmpty())
			return (Patron) this.requestingPatrons.toArray()[0];
		
		return null;
	}
	
	
	/**
	 * Pops the first requesting Patron in the set of requesting Patrons
	 * 
	 */
	
	public synchronized void popPatron(){
		if (!this.requestingPatrons.isEmpty())
			this.requestingPatrons.remove(this.requestingPatrons.toArray()[0]);
	}
	
	

}
