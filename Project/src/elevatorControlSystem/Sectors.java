package elevatorControlSystem;
import java.util.ArrayList;

import elevator.Elevator;


public class Sectors implements DispatcherStrategy {

	@Override
	public Elevator pickElevator(ArrayList<Elevator> elevators, int requestFloor) {
		int elevCount = elevators.size();
		int floorCount = Building.getInstance().getNumberFloors();
		int sectorSize = floorCount / elevCount;
		
		for(int i = 1; i<=elevCount; i++){
			if(requestFloor <= i*sectorSize){
				return elevators.get(i-1);
			}
		}
		//Add an error check later.
		return elevators.get(0); 
		
	}

	@Override
	public void dispatchElevator(Elevator elevator, int destination) {
		elevator.addDestination(destination);
	}

}
