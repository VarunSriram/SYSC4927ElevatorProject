package patron;
import java.util.ArrayList;

import elevator.Elevator;
import elevatorControlSystem.Building;
import elevatorControlSystem.WriteToFile;

public class Waiting implements PatronState {

	@Override
	public void requestElevator(Patron c) {
		// TODO Auto-generated method stub
		c.requestElevator();
	}

	public void entering(Patron c, Elevator e) {
		// TODO Auto-generated method stub
		c.entering(e);
		c.setState(new InElevator());
		WriteToFile.csvAppend("LogPatron.txt","Patron "+c.getId()+" is entering Elevator "+e.getID() +"\n");
	}

	@Override
	public void exiting(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pressFloorButton(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void holdFloorButton(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void arriving(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void continueHolding(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void invokeState(Patron c) {
		// TODO Auto-generated method stub
		this.requestElevator(c);
		ArrayList<Elevator> e = Building.getInstance().getElevator();
		WriteToFile.csvAppend("LogPatron.txt","Patron "+c.getId()+" is waiting for elevator\n");
		
		for(Elevator elev : e){
			if(elev.getCurrentFloor() == c.getStartFloor() && elev.isDoorOpen() && !elev.isFull(c.getWeight())){
				this.entering(c, elev);
				
			}
		
	}

}
}
