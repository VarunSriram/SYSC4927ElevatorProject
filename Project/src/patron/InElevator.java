package patron;

import elevator.Elevator;
import elevatorControlSystem.WriteToFile;

public class InElevator implements PatronState {

	@Override
	public void requestElevator(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void entering(Patron c, Elevator e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exiting(Patron c) {
		
		
	}

	@Override
	public void pressFloorButton(Patron c) {
		// TODO Auto-generated method stub
		c.pressFloorButton();
		c.setState(new Riding());
		WriteToFile.csvAppend("LogPatron.txt","Patron "+c.getId()+" pressed floor button: "+c.getDestinationFloor()+"\n");

	}

	@Override
	public void holdFloorButton(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void arriving(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void continueHolding(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void invokeState(Patron c) {
		// TODO Auto-generated method stub
		this.pressFloorButton(c);
		
	}

}
