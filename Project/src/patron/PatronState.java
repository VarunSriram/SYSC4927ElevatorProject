package patron;

import elevator.Elevator;

public interface PatronState {
	public void invokeState(Patron c);
	public void requestElevator(Patron c);
	public void entering(Patron c, Elevator e);
	public void exiting(Patron c);
	public void pressFloorButton(Patron c);
	public void holdFloorButton(Patron c);
	public void arriving(Patron c);
	public void continueHolding(Patron c);

}
