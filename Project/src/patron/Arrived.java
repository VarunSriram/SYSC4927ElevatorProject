package patron;

import elevator.Elevator;
import elevatorControlSystem.Building;
import elevatorControlSystem.WriteToFile;

public class Arrived implements PatronState {

	@Override
	public void requestElevator(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void entering(Patron c, Elevator e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public synchronized void exiting(Patron c) {
		// TODO Auto-generated method stub
		Elevator e = Building.getInstance().getElevator().get(c.getElevIndex());
		if(e.isDoorOpen()){
			WriteToFile.csvAppend("LogPatron.txt","Patron "+c.getId()+" arrived at Floor: "+c.getDestinationFloor()+"\n");
			c.exiting(e);
			WriteToFile.csvAppend("LogPatron.txt","Patron "+c.getId()+" is exiting Elevator " +e.getID()+ "\n");
		}
		else{
			c.setState(this);
		}
		
	}

	@Override
	public void pressFloorButton(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void holdFloorButton(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void arriving(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void continueHolding(Patron c) {
		// TODO Auto-generated method stub
		c.setState(new InElevator());

	}

	@Override
	public void invokeState(Patron c) {
		// TODO Auto-generated method stub
		this.exiting(c);
	}

}
