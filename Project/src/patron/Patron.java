package patron;
import java.util.ArrayList;


import elevatorControlSystem.*;
import elevator.Elevator;

public class Patron implements Runnable {
	private int startFloor;
	private long originTime;
	private long waitTime;
	private long throughputTime;
	private int destinationFloor;
	private int weight; 
	private PatronState currentState;
	private boolean arrivedFlag;
	private boolean ended;
	private boolean requestFlag;
	private int elevIndex;
	private int id;
	/**
	 * Method used to set a patron's next state
	 * @param s is a PatronState object representing the specific patron state
	 */

	
	public void setState(PatronState s){
		this.currentState = s;
	}
	/**
	 * Method invoked when a patron requests an elevator by pressing elevator button on panel
	 */
	public void requestElevator(){
		if(!requestFlag){
			Building.getInstance().getDispatcher().addRequest(this);
			originTime = Monitor.getInstance().getCurrentTime();
			requestFlag = true;
			WriteToFile.csvAppend("LogPatron.txt","Patron "+this.getId()+" is requesting an elevator\n");
			WriteToFile.csvAppend("LogPatron.txt","Patron "+this.getId()+"'s starting floor: "+this.getStartFloor()+"\n");
			WriteToFile.csvAppend("LogPatron.txt","Patron "+this.getId()+"'s destination floor: "+this.getDestinationFloor()+"\n");
		}
	}
	/**
	 * Method that is invoked when a patron enters an elevator
	 */
	public synchronized void entering(Elevator e){
		ArrayList <Elevator> eList = Building.getInstance().getElevator();
		int i = 0;
		for(Elevator ele : eList){
			if(ele == e){
				this.elevIndex = i;
				break;
			}
			else{
				i++;
			}
		}
		
		waitTime = Monitor.getInstance().getCurrentTime();
		System.out.println("The wait time of patron " + id + " is: " + (waitTime-originTime));
		Monitor.getInstance().addWaitTimeEntry(Math.abs(waitTime-originTime));
		Building.getInstance().removePatron(this, this.startFloor);
		e.addPatron(this);
		
	}
	/**
	 * Method that is invoked when a patron exists an elevator
	 */
	public synchronized void exiting(Elevator e){
		e.removePatron(this);
		throughputTime = Monitor.getInstance().getCurrentTime();
		Monitor.getInstance().addThroughputTimeEntry(Math.abs(throughputTime-originTime));
		this.ended = true;
	}
	/**
	 * Method that is set when an elevator is requested by a patron
	 */
	public void pressFloorButton(){
		Elevator e = Building.getInstance().getElevator().get(this.elevIndex);
		e.addDestination(this.destinationFloor);
		
	}
	/**
	 * Method that is invoked during holding when a patron is holding floor button to close the door
	 */
	public void holdFloorButton(){}
	/**
	 * 
	 */
	public void arriving(){
		//DO SOMETHING
		
		
	}
	public void continueHolding(){}
	/**
	 * @return returns the wait time by a patron for an elevator 
	 */
	public float getWaitTime(){
		return this.waitTime;
	}
	
	public int getStartFloor(){
		return this.startFloor;
		
	}
	
	/**
	 * @return the destination floor that a patron wants to be dropped off at
	 */
	public int getDestinationFloor(){
		return this.destinationFloor;
	}
	/**
	 * @return returns the weight of the patron to compare it to the maximum capacity of the elevator
	 */
	public int getWeight(){
		return this.weight;
	}
	/**
	 * Patron constructor
	 * @param destinationFLoor is the floor the patron wants to get dropped off on
	 * @param startFloor is the floor the patron is currently on
	 * @param weight contains the weight of the patron
	 * @param id contains the id of the patron
	 */
	public Patron(int destinationFLoor,int startFloor, int weight, int id){
		
		if (destinationFLoor != startFloor && destinationFLoor < Building.getInstance().getNumberFloors())
			this.destinationFloor = destinationFLoor;
		
		else
			this.destinationFloor = startFloor - 1;
		
		
		
		if (startFloor < Building.getInstance().getNumberFloors() && startFloor != this.destinationFloor)
			this.startFloor = startFloor;
		
		else
			this.startFloor = this.destinationFloor - 1;
		
		
		
		if (weight >= 20 && weight < 150)
			this.weight = weight;
		
		else
			this.weight = 20;
		
		
		
		this.arrivedFlag = false;
		
		this.ended = false;
		this.requestFlag = false;
		this.id = id;
		this.currentState = new Waiting();
	}

	/**
	 * @return returns the current state of the patron
	 */
	public PatronState getState(){
		return this.currentState;
	}
	/**
	 * @return returns true or false if the patron current state is isWaiting
	 */
	public boolean isWaiting(){
		return this.currentState instanceof Waiting;
	}
	/**
	 * @return returns true or false if the patron's current state is isInElevator 
	 */
	public boolean isInElevator(){
		return this.currentState instanceof InElevator;
	}
	/**
	 * @return returns true or false if the patron's current state is isRiding
	 */
	public boolean isRiding(){
		return this.currentState instanceof Riding;
	}
	/**
	 * @return returns true or false if the patron's current state is hasArrived
	 */
	public boolean hasArrived(){
		return this.currentState instanceof Arrived;
	}
	@Override
	public void run() {
		
		while(!ended){
			this.currentState.invokeState(this);
		}
		
		WriteToFile.csvAppend("LogPatron.txt","Patron " +this.getId() +" Thread terminated\n");
	}
	
	public void tripArrivedFlag(){
		this.arrivedFlag = true;
	}
	
	public boolean hasArrivedFlagTripped(){
		return this.arrivedFlag;
	}
	
	public void setElevIndex(int x){
		this.elevIndex = x;
	}
	
	public int getElevIndex(){
		return this.elevIndex;
	}
	
	public void setEnded(boolean ended){
		this.ended = ended;
	}
	
	public boolean getEnded(){
		return this.ended;
	}
	
	public int getId(){
		return this.id;
	}
	
	public boolean getRequestFlag(){
		return this.requestFlag;
	}

	
}
