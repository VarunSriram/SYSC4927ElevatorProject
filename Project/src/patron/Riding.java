package patron;

import elevator.Elevator;
import elevatorControlSystem.WriteToFile;

public class Riding implements PatronState {

	@Override
	public void requestElevator(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void entering(Patron c, Elevator e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exiting(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pressFloorButton(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void holdFloorButton(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void arriving(Patron c) {
		// TODO Auto-generated method stub
		
		if(c.hasArrivedFlagTripped()){
			c.setState(new Arrived());
		}
		else{
			c.setState(this);
		}

	}

	@Override
	public void continueHolding(Patron c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void invokeState(Patron c) {
		// TODO Auto-generated method stub
		WriteToFile.csvAppend("LogPatron.txt","Patron "+c.getId()+" riding to destination floor: "+c.getDestinationFloor()+"\n");
		this.arriving(c);
	}

}
