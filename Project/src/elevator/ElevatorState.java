package elevator;
/**
 * Elevator state  
 * @author varunsriram
 *
 */
public interface ElevatorState {
public void invokeState(Elevator s);
public void shutDown(Elevator s);
public void getDispatched(Elevator s);
public void waitingInstructions(Elevator s);
public void getNextFloor(Elevator s);
public void goToFloor(Elevator s);
public void closeDoor(Elevator s);
public void openDoor(Elevator s);
public void applyBrake(Elevator s);
public void notifyPatrons(Elevator s);

}
