package elevator;

import elevatorControlSystem.WriteToFile;

public class Ready implements ElevatorState {
	
	@Override
	public void invokeState(Elevator s) {
		// TODO Auto-generated method stub
		
		if (s.getDestinations().size() == 0)
			WriteToFile.csvAppend("LogElevator.txt","Elevator "+s.getID()+" is in IDLE\n");
		
		if (s.getDestinations().size() != 0){
			
			
			this.getNextFloor(s);
			
			if (s.getCurrentFloor() != s.getTargetFloor()){
				s.setState(new Move());//set state to moving.
				WriteToFile.csvAppend("LogElevator.txt","Elevator "+s.getID()+" is being dispatched to Floor: "+s.getTargetFloor()+"\n");
			}
			
			else if (s.getCurrentFloor() == s.getTargetFloor())
				s.setState(new Door());
		}
		
	}

	@Override
	/**
	 * Elevator in Idle State;
	 * @param s Elevator passed in.
	 */
	public void shutDown(Elevator s) {
		// TODO Auto-generated method stub
		s.setState(this);
	}

	@Override
	public void getDispatched(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void waitingInstructions(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getNextFloor(Elevator s) {
		// TODO Auto-generated method stub
		s.getNextFloor();//get the next destination
	}

	@Override
	public void goToFloor(Elevator s) {
		// TODO Auto-generated method stub

	}

	@Override
	public void closeDoor(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void openDoor(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyBrake(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyPatrons(Elevator s) {
		// TODO Auto-generated method stub
		
	}

}
