package elevator;

import elevatorControlSystem.WriteToFile;

public class Door implements ElevatorState {

	@Override
	public void invokeState(Elevator s) {
		// TODO Auto-generated method stub
		this.openDoor(s);
		//DO something.
		this.closeDoor(s);
		this.waitingInstructions(s);
	}

	@Override
	public void shutDown(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getDispatched(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void waitingInstructions(Elevator s) {
		// TODO Auto-generated method stub
		s.setState(new Ready());
	}

	@Override
	public void getNextFloor(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void goToFloor(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeDoor(Elevator s) {
		// TODO Auto-generated method stub
		s.closeDoor();
		WriteToFile.csvAppend("LogElevator.txt","Elevator "+s.getID()+" is closing the door\n");
	}

	@Override
	public void openDoor(Elevator s) {
		// TODO Auto-generated method stub
		s.openDoor();
		WriteToFile.csvAppend("LogElevator.txt","Elevator "+s.getID()+" is opening the door\n");
	}

	@Override
	public void applyBrake(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyPatrons(Elevator s) {
		// TODO Auto-generated method stub
		
	}

}
