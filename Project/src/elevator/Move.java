package elevator;

import elevatorControlSystem.WriteToFile;

public class Move implements ElevatorState {

	@Override
	public void invokeState(Elevator s) {
		// TODO Auto-generated method stub
		this.goToFloor(s);
		
		//System.out.println("Elevator moving");
		WriteToFile.csvAppend("LogElevator.txt","Elevator "+s.getID()+"'s destination floor: " +s.getTargetFloor()+"\n");
		
		
		if(s.getCurrentFloor()== s.getTargetFloor()){
			WriteToFile.csvAppend("LogElevator.txt","Elevator "+s.getID()+" arrived at floor: " +s.getCurrentFloor()+"\n");
			s.setState(new ElevatorArrived());
		}
	}

	@Override
	public void shutDown(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getDispatched(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void waitingInstructions(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getNextFloor(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void goToFloor(Elevator s) {
		// TODO Auto-generated method stub
		s.goToFloor();
	}

	@Override
	public void closeDoor(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void openDoor(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyBrake(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyPatrons(Elevator s) {
		// TODO Auto-generated method stub
		
	}

}
