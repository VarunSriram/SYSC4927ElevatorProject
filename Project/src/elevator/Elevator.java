package elevator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;

import elevatorControlSystem.*;
import patron.*;

public class Elevator extends Observable implements Runnable {
	
	private boolean direction;//true = up false = down
	private float speed;//speed of the elevator
	private long sleepTime; // sleep time of the elevator between each floor
	private boolean doorOpen;//true = open false = closed
	private boolean openingClosing; //true opening/closing
	private int doorMotorSpeed;//speed of the door motor.
	private int maxCapacity;//changed capacity to max Capacity
	private boolean hold;//is the elevator held.
	private ArrayList<Patron> patrons;//list of patrons inside the elevator.
	private volatile ArrayList<Integer> destinations;//list of destination floors the elevator must go to.
	private int targetFloor;// target destination of the elevator.
	private volatile int currentFloor;//current floor the elevator is on.
	private int id;//id of the elevator.
	private ElevatorState currentState;//current state of the elevator state machine.
	private boolean terminated;
	
	public Elevator(float s,int dms,int c,int id){
		this.speed = s;
		this.doorMotorSpeed = dms;
		this.id = id;
		this.maxCapacity = c;
		this.patrons = new ArrayList<Patron>();
		this.destinations = new ArrayList<Integer>();
		this.hold = false;
		this.doorOpen = false;
		this.currentState = new Ready();
		this.terminated = false;
		this.direction = true;
		
		this.currentFloor = 0;
		
	}
	
	public Elevator(float s, int dms, int c, int id, int sFloor){
		this(sFloor, dms, c, id);
		this.currentFloor = sFloor;
	}
	
	
	public void setState(ElevatorState s){
		this.currentState = s;
		if(!(s instanceof Door)){
			System.out.println("Setting State");
			setChanged();
			this.notifyObservers(this.currentState);
		}
	}
	
	public ElevatorState getState(){
		return this.currentState;
	}
	
	public void start(){
		
	}
	
	public void shutDown(){
		this.terminated = true;
	}
	
	public void getDispatched(){
		
	}
	
	public void waitingInstuctions(){
		
	}
	
	public void getNextFloor(){
		this.targetFloor = this.destinations.get(0);
		this.destinations.remove(0);
		if(targetFloor>currentFloor){
			this.goUp();
		}
		else{
			goDown();
		}
	}
	
	public synchronized void goToFloor(){
		if(this.direction){				
			try {
				Thread.sleep((long) this.sleepTime);
				///WriteToFile.csvAppend("LogElevator.txt","Current floor of Elevator in elevator: " +this.currentFloor + "\n");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (this.currentFloor + 1 <= Building.getInstance().getNumberFloors()){
				System.out.println("Moving up");
				this.currentFloor++;
				
			}
		}
		else{
			
			try {
				Thread.sleep((long) this.sleepTime);
				
				//WriteToFile.csvAppend("LogElevator.txt","Current floor of Elevator in elevator: " +this.currentFloor + "\n");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if (this.currentFloor - 1 > -1){
				System.out.println("Moving down");
				--this.currentFloor;
			}
			
			
		}
		setChanged();
		this.notifyObservers(this.currentFloor);
	}
	
	public void closeDoor(){
		this.doorOpen = false;
		this.openingClosing = false;
		setChanged();
		this.notifyObservers(this.currentState);
		try {
			Thread.sleep(doorMotorSpeed);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void openDoor(){
		this.openingClosing = true;
		setChanged();
		this.notifyObservers(this.currentState);
		try {
			Thread.sleep(doorMotorSpeed);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.doorOpen = true;
		setChanged();
		this.notifyObservers(this.currentState);
		while(!this.isFloorCleared()){
			try {
				Thread.sleep(doorMotorSpeed);
				System.out.println("The elevator " + this.id + " is stuck");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			Thread.sleep(doorMotorSpeed*3l);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	public void applyBrake(){
		

	}
	
	public void notifyPatrons(){
		for(Patron p : patrons ){
			if(p.getDestinationFloor()==this.currentFloor){
				p.tripArrivedFlag();
			}
		}
	}
	
	public synchronized void goDown(){
		
		
		
		this.direction = false;//false means down direction
		//Collections.sort(destinations, Collections.reverseOrder());//orders the destinations in descending order.
	}
	
	public synchronized void goUp(){
		
		this.direction = true;//true means up direction
		//Collections.sort(destinations);//orders the destinations in acending order
	}
	
	public float getSpeed(){
		return this.speed;
	}
	
	public int getCapacity(){
		return this.patrons.size();
	}
	
	public boolean isFull(int addedWeight){
		int sum = 0;
		for(Patron p : patrons){
			sum+= p.getWeight();
		}
		return sum+addedWeight >= this.maxCapacity; 
	}
	
	public boolean isFull(){
		int sum = 0;
		for(Patron p : patrons){
			sum+= p.getWeight();
		}
		return maxCapacity - sum < 20;
	}
	
	public boolean isHeld(){
		return this.hold;
	}
	
	public boolean isReady(){
		return this.currentState instanceof Ready ;
	}
	
	
	public boolean isMoving(){
		return this.currentState instanceof Move;
	}
	
	public boolean hasArrived(){
		return this.currentState instanceof ElevatorArrived;
	}
	public boolean isTerminated(){
		return this.terminated;
	}
	
	public boolean isDoorActive(){
		return this.currentState instanceof Door ;
	}
	
	public boolean getDirection(){
		return this.direction;
	}
	
	public int getCurrentFloor(){
		return this.currentFloor;
	}
	
	public int getTargetFloor(){
		return this.targetFloor;
	}
	
	public ArrayList<Integer> getDestinations(){
		return this.destinations;
	}
	public synchronized void addDestination(int x){
		if(!destinations.contains(x)){
			this.destinations.add(x);
			if(this.direction){
				//sort
				ArrayList<Integer> aboveList = new ArrayList<Integer>();
				ArrayList<Integer> belowList = new ArrayList<Integer>();
			
				for(Integer i : this.destinations){
					if (i < this.currentFloor){
						belowList.add(i);
					}
					else{
						aboveList.add(i);
					}
				}
				Collections.sort(aboveList);//orders the destinations in descending order.
				Collections.sort(belowList,Collections.reverseOrder());
				this.destinations.clear();
				for(Integer i : aboveList){
					this.destinations.add(i);
					System.out.print(i+" ");
				}
				
				for(Integer i : belowList){
					this.destinations.add(i);
					System.out.print(i+" ");
				}
				System.out.println("");
				
			}
			else{
				ArrayList<Integer> aboveList = new ArrayList<Integer>();
				ArrayList<Integer> belowList = new ArrayList<Integer>();
			
				for(Integer i : this.destinations){
					if (i < this.currentFloor){
						belowList.add(i);
					}
					else{
						aboveList.add(i);
					}
				}
				Collections.sort(belowList, Collections.reverseOrder());//orders the destinations in descending order.
				Collections.sort(aboveList);
				this.destinations.clear();
				for(Integer i : belowList){
					this.destinations.add(i);
					System.out.print(i+" ");
				}
				
				for(Integer i : aboveList){
					this.destinations.add(i);
					System.out.print(i+" ");
				}
				System.out.println("");
			}
		}
		
		
	}
	public synchronized void addPatron(Patron p){
		this.patrons.add(p);
	}
	
	public synchronized void removePatron(Patron p){
		this.patrons.remove(p);
	}
	
	public ArrayList<Patron> getPatrons(){
		return this.patrons;
	}
	
	public boolean isDoorOpen(){
		return doorOpen;
	}
	
	public boolean isDoorOpening(){
		return this.openingClosing;
	}
	
	public void terminateElevator(boolean b){
		this.terminated = b;
	}
	
	public boolean isFloorCleared(){
		int leavingPatronCount =0;
		ArrayList<Patron> waitingP = Building.getInstance().getPatronAtFloor(this.targetFloor);
		for(Patron p : patrons){
			if(p.getDestinationFloor() == this.targetFloor){
				leavingPatronCount++;
			}
		}
		
		if((leavingPatronCount == 0)){
			for (Patron p: patrons){
				if(waitingP.size() !=0){
					if(this.isFull()){
						return true;
					}
					else if (this.isFull(p.getWeight())){
						return true;
					}
					
					else{
						return false;
					}
				}
				else{
					return true;
				}
				
			}
		}
		
		
		return true;		
	}
	
	public int getID(){
		return this.id;
	}
	
	
	
	@Override
	public void run() {
		this.sleepTime = (long)(Building.getInstance().getFloorDistance() / this.speed)*1000l;
		
		// TODO Auto-generated method stub
		while(!terminated){
			this.currentState.invokeState(this);
			
		}
	}
	
	public void setChangedState(){
		this.setChanged();
	}
	
	
	

}
