package elevator;

import elevatorControlSystem.WriteToFile;

public class ElevatorArrived implements ElevatorState {

	@Override
	public void invokeState(Elevator s) {
		// TODO Auto-generated method stub
		this.notifyPatrons(s);
		s.setState(new Door());
	}

	@Override
	public void shutDown(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getDispatched(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void waitingInstructions(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getNextFloor(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void goToFloor(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeDoor(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void openDoor(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyBrake(Elevator s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyPatrons(Elevator s) {
		// TODO Auto-generated method stub
		//DO set patron flags to true to notify them.
		s.notifyPatrons();
		WriteToFile.csvAppend("LogElevator.txt","Elevator "+s.getID()+" is notifying the patrons of arrival\n");
	}

}
