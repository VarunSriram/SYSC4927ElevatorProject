package main;

import java.util.ArrayList;

import elevatorControlSystem.*;
import guiMVC.OperatorGUI;
import runScripts.RunScripts;

public class Main {

	public static void main(String[] args) {
		
		
		//RunScripts.runScripts("graphScript.sh");
		
		
		Simulator simulator = new Simulator();
		
		int capacity = 0,
		    doorSpeed = 0, 
		    maxPatrons = 0,
		    numberOfFloors = 0,
		    numberOfElevators = 0;
		    
		
		float speed = 0.0f,
		      pollTime = 0.0f,
		      floorDistance = 0.0f;
		   
		DispatcherStrategy strategy = null;
		
		
		if (args.length > 0) {
		    try {
		        maxPatrons = Integer.parseInt(args[0]);
		        
		        if (maxPatrons <= 0)
		        	maxPatrons = 1;
		        
		        numberOfFloors = Integer.parseInt(args[1]);
		        
		        if (numberOfFloors <= 0)
		        	numberOfFloors = 10;
		        
		        numberOfElevators = Integer.parseInt(args[2]);
		        
		        if (numberOfElevators <= 0)
		        	numberOfElevators = 2;
		        
		        floorDistance = Float.parseFloat(args[3]);
		        
		        if (floorDistance < 5.0f)
		        	floorDistance = 10.0f;
		        
		        pollTime = Float.parseFloat(args[4]);
		        
		        if (pollTime < 0.2f)
		        	pollTime = 1.0f;
		        
		        speed = Float.parseFloat(args[5]);
		        
		        if (speed < 1.0f)
		        	speed = 1.0f;
		        
		        doorSpeed = Integer.parseInt(args[6]);
		        
		        if (doorSpeed <= 999)
		        	doorSpeed = 1000;
		        
		        capacity = Integer.parseInt(args[7]);
		        
		        if (capacity <= 250)
		        	capacity = 500;
		        
		    } catch (NumberFormatException e) {
		        System.err.println("First seven arguments must be numbers");
		        System.exit(1);
		    }
		    
		    
		    if (args[8].equalsIgnoreCase("Sectors"))
				strategy = new Sectors();
			
			else if (args[8].equalsIgnoreCase("NearestElevator"))
				strategy = new NearestElevator();
			
			else if (args[8].equalsIgnoreCase("Automatic") || strategy == null){
				ArrayList<DispatcherStrategy> strategies = new ArrayList<DispatcherStrategy>();
				strategies.add(new Sectors());
				strategies.add(new NearestElevator());
				
				strategy = new Automatic(strategies);
			}
		    
		    simulator.setupSim(maxPatrons, numberOfFloors, numberOfElevators, floorDistance, pollTime, speed, doorSpeed, capacity, strategy);
			simulator.runSim();
		}
		
		else{
			strategy = new Sectors();
			//strategy = new NearestElevator();
			
			//ArrayList<DispatcherStrategy> strategies = new ArrayList<DispatcherStrategy>();
			//strategies.add(new Sectors());
			//strategies.add(new NearestElevator());
			
			//strategy = new Automatic(strategies);
			
			simulator.setupSim(10, 12, 4, 10.5f, 5.0f, 6, 1000, 1500, strategy);
			simulator.runSim();
		}

		    
		
		

		

	}

}
