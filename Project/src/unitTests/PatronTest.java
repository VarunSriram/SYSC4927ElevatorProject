
package unitTests;

import elevatorControlSystem.*;
import patron.*;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PatronTest {
	
	private Patron patron;

	@Before
	public void setUp() throws Exception {
		Building.getInstance().setup(11, 10.5f, null, 0.5f, new Sectors());
		this.patron = new Patron(10, 0, 30, 0);
	}

	@After
	public void tearDown() throws Exception {
		this.patron = null;
	}

	@Test
	public void testSetState() {
		this.patron.setState(new Riding());
		assertEquals(new Riding().getClass(), this.patron.getState().getClass());
		
		this.patron.setState(new InElevator());
		assertEquals(new InElevator().getClass(), this.patron.getState().getClass());
		
		this.patron.setState(new Arrived());
		assertEquals(new Arrived().getClass(), this.patron.getState().getClass());
	}

	@Test
	public void testRequestElevator() {
		assertFalse(this.patron.getRequestFlag());
		this.patron.requestElevator();
		assertTrue(this.patron.getRequestFlag());
	}
	
	@Test
	public void testGetRequestFlag() {
		assertFalse(this.patron.getRequestFlag());
	}

	@Test
	public void testGetStartFloor() {
		assertEquals(0, this.patron.getStartFloor());
		
		Building.getInstance().setup(11, 10.5f, null, 0.1f, new Sectors());
		this.patron = new Patron(0, 10, 40, 0);
		assertEquals(10, this.patron.getStartFloor());
		
		Building.getInstance().setup(20, 10.5f, null, 0.1f, new Sectors());
		this.patron = new Patron(0, 19, 19, 0);
		assertEquals(19, this.patron.getStartFloor());
		
		this.patron = new Patron(0, -1, 20, 0);
		assertEquals(-1, this.patron.getStartFloor());
		
		Building.getInstance().setup(5, 10.5f, null, 0.1f, new Sectors());
		this.patron = new Patron(4, 5, 190, 0);
		assertEquals(3, this.patron.getStartFloor());
	}

	@Test
	public void testGetDestinationFloor() {
		assertEquals(10, this.patron.getDestinationFloor());
		
		Building.getInstance().setup(11, 10.5f, null, 0.1f, new Sectors());
		this.patron = new Patron(0, 10, 40, 0);
		assertEquals(0, this.patron.getDestinationFloor());
		
		Building.getInstance().setup(20, 10.5f, null, 0.1f, new Sectors());
		this.patron = new Patron(0, 19, 19, 0);
		assertEquals(0, this.patron.getDestinationFloor());
		
		this.patron = new Patron(0, -1, 20, 0);
		assertEquals(0, this.patron.getDestinationFloor());
		
		Building.getInstance().setup(5, 10.5f, null, 0.1f, new Sectors());
		this.patron = new Patron(4, 5, 190, 0);
		assertEquals(4, this.patron.getDestinationFloor());
	}

	@Test
	public void testGetWeight() {
		assertEquals(30, this.patron.getWeight());
		
		this.patron = new Patron(0, 0, 40, 0);
		assertEquals(40, this.patron.getWeight());
		
		this.patron = new Patron(0, 0, 19, 0);
		assertEquals(20, this.patron.getWeight());
		
		this.patron = new Patron(0, 0, -1, 0);
		assertEquals(20, this.patron.getWeight());
		
		this.patron = new Patron(0, 0, 190, 0);
		assertEquals(20, this.patron.getWeight());
		
		this.patron = new Patron(0, 0, 150, 0);
		assertEquals(20, this.patron.getWeight());
		
		this.patron = new Patron(0, 0, 149, 0);
		assertEquals(149, this.patron.getWeight());
	}

	@Test
	public void testPatron() {
		assertEquals(Patron.class, this.patron.getClass());
	}

	@Test
	public void testGetState() {
		assertEquals(new Waiting().getClass(), this.patron.getState().getClass());
	}

	@Test
	public void testIsWaiting() {
		assertTrue(this.patron.isWaiting());
		
		this.patron.setState(new Riding());
		assertFalse(this.patron.isWaiting());
		
		this.patron.setState(new InElevator());
		assertFalse(this.patron.isWaiting());
		
		this.patron.setState(new Arrived());
		assertFalse(this.patron.isWaiting());
		
	}

	@Test
	public void testIsInElevator() {
		assertFalse(this.patron.isInElevator());
		
		this.patron.setState(new Riding());
		assertFalse(this.patron.isInElevator());
		
		this.patron.setState(new InElevator());
		assertTrue(this.patron.isInElevator());
		
		this.patron.setState(new Arrived());
		assertFalse(this.patron.isInElevator());
	}

	@Test
	public void testIsRiding() {
		assertFalse(this.patron.isRiding());
		
		this.patron.setState(new Riding());
		assertTrue(this.patron.isRiding());
		
		this.patron.setState(new InElevator());
		assertFalse(this.patron.isRiding());
		
		this.patron.setState(new Arrived());
		assertFalse(this.patron.isRiding());
	}

	@Test
	public void testHasArrived() {
		assertFalse(this.patron.hasArrived());
		
		this.patron.setState(new Riding());
		assertFalse(this.patron.hasArrived());
		
		this.patron.setState(new InElevator());
		assertFalse(this.patron.hasArrived());
		
		this.patron.setState(new Arrived());
		assertTrue(this.patron.hasArrived());
	}

	@Test
	public void testTripArrivedFlag() {
		assertFalse(this.patron.hasArrivedFlagTripped());
		
		this.patron.tripArrivedFlag();
		
		assertTrue(this.patron.hasArrivedFlagTripped());
	}

	@Test
	public void testHasArrivedFlagTripped() {
		assertFalse(this.patron.hasArrivedFlagTripped());
	}

	@Test
	public void testSetEnded() {
		assertFalse(this.patron.getEnded());
		
		this.patron.setEnded(true);
		assertTrue(this.patron.getEnded());
	}
	
	@Test
	public void testGetEnded() {
		assertFalse(this.patron.getEnded());
	}

	@Test
	public void testGetId() {
		assertEquals(0, this.patron.getId());
		
		this.patron = new Patron(0, 0, 0, 1);
		assertEquals(1, this.patron.getId());
		
		this.patron = new Patron(0, 0, 0, -1);
		assertEquals(-1, this.patron.getId());
		
		this.patron = new Patron(0, 0, 0, 100);
		assertEquals(100, this.patron.getId());
		
		this.patron = new Patron(0, 0, 0, -1001);
		assertEquals(-1001, this.patron.getId());
	}

}
