
package unitTests;

import elevatorControlSystem.*;
import patron.Patron;
import elevator.*;

import java.util.ArrayList;


import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;



public class ElevatorTest {
	
	private static final int ID = 1,
			                 CAPACITY = 1000,
			                 DOORMOTORSPEED = 2;

	private static final float SPEED = 2.0f,
			                   DELTAFLOAT = 0.000001f;
	
	private Elevator elevator;

	@Before
	public void setUp() throws Exception {
		this.elevator = new Elevator(SPEED, DOORMOTORSPEED, CAPACITY, ID);
	}

	@After
	public void tearDown() throws Exception {
		this.elevator = null;
	}

	@Test
	public void testElevator() {
		assertEquals(Elevator.class, this.elevator.getClass());
	}

	@Test
	public void testSetState() {
		this.elevator.setState(new Move());
		assertEquals(new Move().getClass(), this.elevator.getState().getClass());
		
		this.elevator.setState(new ElevatorArrived());
		assertEquals(new ElevatorArrived().getClass(), this.elevator.getState().getClass());
		
		this.elevator.setState(new Door());
		assertEquals(new Door().getClass(), this.elevator.getState().getClass());
		
		this.elevator.setState(new Ready());
		assertEquals(new Ready().getClass(), this.elevator.getState().getClass());
	}

	@Test
	public void testGetState() {
		assertEquals(new Ready().getClass(), this.elevator.getState().getClass());
	}

	@Test
	public void testShutDown() {
		this.elevator.shutDown();
		assertTrue(this.elevator.isTerminated());
		
	}

	@Test
	public void testGetNextFloor() {
		
		for (int i = 1; i <= 10; i++){
			this.elevator.addDestination(i);
			this.elevator.getNextFloor();
			assertEquals(i, this.elevator.getTargetFloor());
		}
		
	}

	@Test
	public void testGoToFloor() {
		for (int i = 1; i <= 10; i++){
			this.elevator.addDestination(i);
			this.elevator.getNextFloor();
			this.elevator.goToFloor();
			assertEquals(i, this.elevator.getTargetFloor());
		}
	}

	@Test
	public void testCloseDoor() {
		this.elevator.closeDoor();
		assertFalse(this.elevator.isDoorOpen());
	}

	@Test
	public void testOpenDoor() {
		this.elevator.openDoor();
		assertTrue(this.elevator.isDoorOpen());
	}

	@Test
	public void testGoDown() {
		this.elevator.goDown();
		assertFalse(this.elevator.getDirection());
	}

	@Test
	public void testGoUp() {
		Building.getInstance().setup(5, 10.0f, new ArrayList<Elevator>(), 0.5f, new Sectors());
		this.elevator.goUp();
		assertTrue(this.elevator.getDirection());
		
	}

	@Test
	public void testGetSpeed() {
		assertEquals(SPEED, this.elevator.getSpeed(), DELTAFLOAT);
		
		this.elevator = new Elevator(0.1f, DOORMOTORSPEED,CAPACITY,ID);
		assertEquals(0.1f, this.elevator.getSpeed(), DELTAFLOAT);
		
		this.elevator = new Elevator(110.001f, DOORMOTORSPEED,CAPACITY,ID);
		assertEquals(110.001f, this.elevator.getSpeed(), DELTAFLOAT);
	}

	@Test
	public void testGetCapacity() {
		assertEquals(0, this.elevator.getCapacity());
		
		for (int i = 1; i < 10; i++){
			this.elevator.addPatron(new Patron(0, 0, 1, 0));
		
			assertEquals(i, this.elevator.getCapacity());
		}
		
	}

	@Test
	public void testIsFullInt() {
		assertTrue(this.elevator.isFull(CAPACITY));
		
		assertFalse(this.elevator.isFull(0));
	}

	@Test
	public void testIsFull() {
		assertFalse(this.elevator.isFull());
	}

	@Test
	public void testIsHeld() {
		assertFalse(this.elevator.isHeld());
		
	}

	@Test
	public void testIsReady() {
		assertTrue(this.elevator.isReady());
		
		this.elevator.setState(new Move());
		assertFalse(this.elevator.isReady());
		
		this.elevator.setState(new ElevatorArrived());
		assertFalse(this.elevator.isReady());
		
		this.elevator.setState(new Ready());
		assertTrue(this.elevator.isReady());
	}

	@Test
	public void testIsMoving() {
		assertFalse(this.elevator.isMoving());
		
		this.elevator.setState(new Move());
		assertTrue(this.elevator.isMoving());
		
		this.elevator.setState(new ElevatorArrived());
		assertFalse(this.elevator.isMoving());
		
		this.elevator.setState(new Ready());
		assertFalse(this.elevator.isMoving());
	}

	@Test
	public void testHasArrived() {
		this.elevator.setState(new ElevatorArrived());
		assertTrue(this.elevator.hasArrived());
		
		this.elevator.setState(new Move());
		assertFalse(this.elevator.hasArrived());
		
		this.elevator.setState(new Door());
		assertFalse(this.elevator.hasArrived());
	}

	@Test
	public void testIsDoorActive() {
		assertFalse(this.elevator.isDoorActive());
		
		this.elevator.setState(new Door());
		assertTrue(this.elevator.isDoorActive());
	}

	@Test
	public void testGetDirection() {
		assertTrue(this.elevator.getDirection());
		
		this.elevator.goUp();
		assertTrue(this.elevator.getDirection());
		
		this.elevator.goDown();
		assertFalse(this.elevator.getDirection());
		
	}

	@Test
	public void testGetCurrentFloor() {
		assertEquals(0, this.elevator.getCurrentFloor());
	}

	@Test
	public void testGetTargetFloor() {
		assertEquals(0, this.elevator.getTargetFloor());
	}

	@Test
	public void testGetDestinations() {
		assertEquals(0, this.elevator.getDestinations().size());
		
		for (int i = 1; i <= 10; i++){
			this.elevator.addDestination(i);
			assertEquals(i, this.elevator.getDestinations().size());
		}
	}

	@Test
	public void testAddDestination() {
		this.elevator.addDestination(1);
		assertEquals(1, this.elevator.getDestinations().size());
		
		for (int i = 2; i <= 20; i++){
			this.elevator.addDestination(i);
			assertEquals(i, this.elevator.getDestinations().size());
		}
	}

	@Test
	public void testAddPatron() {
		assertEquals(0, this.elevator.getPatrons().size());
		
		for (int i = 1; i <= 10; i++){
			this.elevator.addPatron(new Patron(i, i, i, i));
			assertEquals(i, this.elevator.getPatrons().size());
		}
	}

	@Test
	public void testRemovePatron() {
		for (int i = 1; i < 10; i++){
			this.elevator.addPatron(new Patron(i, i, i, i));
			assertEquals(i, this.elevator.getPatrons().size());
		}
		
		int size = this.elevator.getPatrons().size();
		
		
		for (int i = 0; i < size; i++){
			size -= 1;
			this.elevator.removePatron(this.elevator.getPatrons().get(0));
			assertEquals(size, this.elevator.getPatrons().size());
		}
		
		
	}

	@Test
	public void testGetPatrons() {
		assertEquals(0, this.elevator.getPatrons().size());
	}

	@Test
	public void testIsDoorOpen() {
		assertFalse(this.elevator.isDoorOpen());
	}

	@Test
	public void testTerminateElevator() {
		assertFalse(this.elevator.isTerminated());
		
		this.elevator.terminateElevator(true);
		assertTrue(this.elevator.isTerminated());
		
		this.elevator.terminateElevator(false);
		assertFalse(this.elevator.isTerminated());
	}

	@Test
	public void testIsFloorCleared() {
		Building.getInstance().setup(5, 10.0f, new ArrayList<Elevator>(), 0.5f, new Sectors());
		assertTrue(this.elevator.isFloorCleared());
	}

	@Test
	public void testGetID() {
		assertEquals(ID, this.elevator.getID());
		
		this.elevator = new Elevator(SPEED, DOORMOTORSPEED,CAPACITY,10);
		assertEquals(10, this.elevator.getID());
		
		this.elevator = new Elevator(SPEED, DOORMOTORSPEED,CAPACITY, 20);
		assertEquals(20, this.elevator.getID());
	}
	
	@Test
	public void testIsDoorOpening(){
		assertFalse(this.elevator.isDoorOpening());
		
		this.elevator.openDoor();
		
		assertTrue(this.elevator.isDoorOpening());
		
		this.elevator.closeDoor();
		
		assertFalse(this.elevator.isDoorOpening());
	}

 }
