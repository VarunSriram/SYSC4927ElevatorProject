package guiMVC;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import elevator.Door;
import elevator.Elevator;
import elevator.ElevatorState;
import elevator.Move;
import elevator.Ready;
import patron.Arrived;

public class ElevatorTracker extends JFrame implements Observer {
	
	private static final long serialVersionUID = 8898486145896003490L;
	private JPanel[][] grid;
	  private JFrame main;
	  int elevatorNum;
	  int x = 4;
	  
	  public ElevatorTracker(int numberOfElevators, int xOfNextWindow, int yOfNextWindow, int sizeOfNextWindow){
		  	int gridSizex = numberOfElevators+1;
			int gridSizey = x;
			JPanel board = new JPanel(new GridLayout (gridSizex, gridSizey));
			grid = new JPanel[gridSizex][gridSizey];
			main = new JFrame("Elevator Monitor--Number of Elevators: "+numberOfElevators);
			Container c = main.getContentPane();
			
			
			
			for (int i = 0; i < gridSizex; i++){
				  for (int a = 0; a< gridSizey; a++){
					  
					  
					  
					JPanel jb = new JPanel();
					jb.setBackground(new Color(0.1f,0.1f,0.1f));
					grid[i][a] = jb;
					grid[i][a].add(new JLabel());
					grid[i][a].getComponent(0).setForeground(Color.WHITE);
					board.add(grid[i][a]);
					
				  }
			}
			
			String[] headings = {"ID","State","Patron Count", "At Capacity?"};
			for(int i = 0; i <gridSizey;i++){
				JLabel jl = (JLabel) this.grid[0][i].getComponent(0);
				jl.setText(headings[i]);
				
			}
			
			
			for(int i = 1; i<gridSizex;i++){
				 JLabel jl = (JLabel) this.grid[i][0].getComponent(0);
				  jl.setText(Integer.toString(i-1));
			}
			
			
			
			
			c.add(board, "Center");
			board.setPreferredSize(new Dimension(500,300));
			
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			main.setLocation((int) (dim.getWidth()*0.5), (int) (dim.getHeight()*0));

			main.pack();
			main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			main.setVisible(true);
			
			
			
	  }
	  
	  public void setState(int id, int state){
		  switch(state){
		  case 0: {
			  JLabel jl = (JLabel) this.grid[id+1][1].getComponent(0);
			  jl.setText("Ready");
			  break;
		  }
		  
		  case 1: {
			  JLabel jl = (JLabel) this.grid[id+1][1].getComponent(0);
			  jl.setText("Moving");
			  break;
		  }
		  
		  case 2: {
			  JLabel jl = (JLabel) this.grid[id+1][1].getComponent(0);
			  jl.setText("Arrived");
			  break;
		  }
		  
		  case 3: {
			  JLabel jl = (JLabel) this.grid[id+1][1].getComponent(0);
			  jl.setText("Door");
			  break;
		  }
		  
		  default: {
			  JLabel jl = (JLabel) this.grid[id+1][1].getComponent(0);
			  jl.setText("");
			  break;
		  }
		  
		  }
		  
	  }
	  
	  public void setPatronCount(int id, int count){
		  JLabel jl = (JLabel) this.grid[id+1][2].getComponent(0);
		  jl.setText(Integer.toString(count));
	  }
	  
	  public void setIsFull(int id, boolean full){
		  JLabel jl = (JLabel) this.grid[id+1][3].getComponent(0);
		  jl.setText(Boolean.toString(full));
	  }
	  
	 
	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		if(arg0 instanceof Elevator){
			int id = ((Elevator)arg0).getID();
			int stateVal = 500;
			ElevatorState es = ((Elevator)arg0).getState();
			if(es instanceof Ready){
				stateVal = 0;
			}
			else if(es instanceof Move){
				stateVal = 1;
			}
			else if(es instanceof Arrived){
				stateVal = 2;
			}
			else if (es instanceof Door){
				stateVal = 3;
			}
			
			
			int patronCount = ((Elevator)arg0).getCapacity();
			boolean isFull = ((Elevator)arg0).isFull();
			
			this.setState(id,stateVal);
			this.setPatronCount(id,patronCount);
			this.setIsFull(id,isFull);
			
			
		}
	}

}
