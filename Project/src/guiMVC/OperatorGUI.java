package guiMVC;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import runScripts.RunScripts;

public class OperatorGUI extends JFrame {

	private static final long serialVersionUID = 4102771667663435263L;
	JButton viewGraphs;
	
	File file1,
	     file2,
	     file3,
	     image1,
	     image2,
	     image3;
	
	public OperatorGUI(){
		
		file1 = new File("TotalWaitTime.csv");
		file2 = new File("AverageWaitTime.csv");
		file3 = new File("TotalThroughputTime.csv");
		image1 = new File("TotalWaitTime.png");
		image2 = new File("AverageWaitTime.png");
		image3 = new File("TotalThroughputTime.png");
		
		
		
		this.setTitle("Operator GUI");
		
		viewGraphs = new JButton("View Graphs");
		
		viewGraphs.setEnabled(false);
		
		viewGraphs.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				    RunScripts.runScripts("graphScript.sh");
				    int count = 0;
				    //while(!image1.exists() && !image2.exists() && !image3.exists())
				    	//count ++;
				    
				    DisplayImage image1 = new DisplayImage("TotalWaitTime.png");
				    DisplayImage image2 = new DisplayImage("AverageWaitTime.png");
				    DisplayImage image3 = new DisplayImage("TotalThroughputTime.png");
				  } 
				} );
		
		
		JPanel panel = new JPanel();
		
		panel.add(viewGraphs);
		
		this.getContentPane().add(panel);
		
		this.setSize(200, 200);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((int) (dim.getWidth()*0.80), (int) (dim.getHeight()*0));
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setVisible(true);
		
	}
	
	public void enableButton(){
		if (file1.exists() && file2.exists() && file3.exists())
			viewGraphs.setEnabled(true);
	}

}
