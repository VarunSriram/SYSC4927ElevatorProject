package guiMVC;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class DisplayImage extends JFrame {
	
	private static final long serialVersionUID = 3820585533898496122L;
	
	private static final double RATIO = 1;
	
	public DisplayImage(String fileName){
		
		BufferedImage originalImage = null;
		
		try {
			originalImage = ImageIO.read(new File(fileName));
			int type = originalImage.getType();
			
			BufferedImage resizedImage = this.resizeImage(originalImage, RATIO, type);
			
			String newFileName = fileName.substring(0, fileName.length() - 4) + originalImage.getHeight()*RATIO + "x" + originalImage.getWidth()*RATIO;
			
			ImageIO.write(resizedImage, "png", new File(newFileName));
			
			ImageIcon icon = new ImageIcon(fileName);
			
			this.setSize(960, 540);
			
			JPanel panel = new JPanel();
			JLabel label = new JLabel();
			
			label.setIcon(icon);
			panel.add(label);
			
			this.getContentPane().add(panel);
			this.setTitle(fileName.substring(0, fileName.length() - 4));
			
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			this.setLocation((int) (dim.getWidth()*0.5), (int) (dim.getHeight()*0.5));
			
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setVisible(true);
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
	}
	
	private BufferedImage resizeImage(BufferedImage originalImage, double ratio, int type){
		BufferedImage resizedImage = new BufferedImage((int)(originalImage.getHeight()*ratio), (int)(originalImage.getWidth()*ratio), type);
		Graphics2D graphics = resizedImage.createGraphics();
		graphics.drawImage(originalImage, 0, 0, (int)(originalImage.getHeight()*ratio), (int)(originalImage.getWidth()*ratio), null);
		graphics.dispose();
		return resizedImage;
	}

}
