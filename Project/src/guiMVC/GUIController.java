package guiMVC;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;

import elevator.Elevator;
import elevator.Ready;
import elevatorControlSystem.Building;
public class GUIController {
private ElevGrid view;
private Building model;
private ElevatorTracker elevView;

public GUIController(ElevGrid view,ElevatorTracker view1){
	this.view = view;
	this.model = Building.getInstance();
	this.elevView = view1;
}

public void setupGUI(){
	

	ArrayList<Elevator> elevators = this.model.getElevator();
	
	
	for(Elevator e : elevators){
		e.addObserver(this.view);
		e.addObserver(this.elevView);
		if(e.getState() instanceof Ready){
			System.out.println("HEELOOO");
		}
		e.setChangedState();
		e.notifyObservers(e.getState());
	}
	this.model.addObserver(this.view);
	
}



}
