package guiMVC;
import javax.swing.*;

import elevator.Door;
import elevator.Elevator;
import elevator.ElevatorState;
import elevator.Move;
import elevator.Ready;
import elevatorControlSystem.*;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class ElevGrid extends JFrame implements Observer{
	private static final long serialVersionUID = 3470563104744377978L;
	
	private JPanel[][] grid;
	  private JFrame building;
	  private int gridSizex;
	  private int gridSizey;
	  
	 public ElevGrid(int boardsizeX, int boardsizeY) {
		gridSizex = boardsizeX+1;
		gridSizey = boardsizeY+1;
		JPanel board = new JPanel(new GridLayout (gridSizex, gridSizey));
		grid = new JPanel[gridSizex][gridSizey];
		building = new JFrame("Elevator Grid- " + "No. of Elevators: "+boardsizeY+" No.of Floors"+boardsizeX);
		Container c = building.getContentPane();
		
		
		
		
		
		for (int i = 0; i < gridSizex; i++){
		  for (int a = 0; a< gridSizey; a++){
			  
			  
			  
			JPanel jb = new JPanel();
			jb.setBackground(new Color(0.1f,0.1f,0.1f));
			grid[i][a] = jb;
			board.add(grid[i][a]);
		  }
		}
		
		for(int i = 1; i <gridSizey;i++){
			grid[0][i].add(new JLabel("H"));
			grid[0][i].getComponent(0).setForeground(Color.WHITE);
			
		}
		
		for(int i = 1; i <gridSizex;i++){
			grid[i][0].add(new JLabel("N"));
			grid[i][0].getComponent(0).setForeground(Color.YELLOW);
		}
		
		c.add(board, "Center");
		board.setPreferredSize(new Dimension(1000,1000));
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		building.setLocation(0, 0);

		building.pack();
		building.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		building.setVisible(true);
	  }
	  
	  public void setClosed(int x, int y){
		  this.grid[x+1][y+1].setBackground(Color.BLACK);
	  }
	  
	  public void setClosing(int x, int y){
		  this.grid[x+1][y+1].setBackground(Color.RED);
	  }
	  
	  public void setOpening(int x, int y){
		  this.grid[x+1][y+1].setBackground(Color.GREEN);
	  }
	  
	  public void setOpened(int x, int y){
		  this.grid[x+1][y+1].setBackground(Color.WHITE);
	  }
	  
	  public void setReady(int x, int y){
		  this.grid[x+1][y+1].setBackground(Color.MAGENTA);
	  }
	  public void setFloorLamp(int elevator, int floor){
		 JLabel jl = (JLabel) this.grid[0][elevator+1].getComponent(0);
		 jl.setText(Integer.toString(floor));
	  }
	  
	  public void setPatronWaitingNumber(int floor, int patronCount){
			 JLabel jl = (JLabel) this.grid[floor+1][0].getComponent(0);
			 jl.setText(Integer.toString(patronCount));
		  }
	  public void refresh(){
		  this.building.setVisible(false);
		  this.building.setVisible(true);
	  }
	  
	  public synchronized void clearColumn(int elev){
		  
			  for(int j =1; j< this.gridSizex;j++){
				  System.out.println("COLUMN"+j);
				  System.out.println(elev+1);
				  this.grid[j][elev+1].setBackground(new Color(0.1f,0.1f,0.1f));
			  }
		  
	  }
	  
	  public void resetBoard(){
		  for(int i = 1; i< this.gridSizey; i++){
			  for(int j =1; j< this.gridSizey;j++){
				  this.grid[i][j].setBackground(new Color(0.1f,0.1f,0.1f));
			  }	
		  }
	  }
	  
	  
	

@Override
public void update(Observable arg0, Object arg1) {
	if(arg0 instanceof Building){
		this.setPatronWaitingNumber((Integer) arg1, Building.getInstance().getPatronAtFloor((Integer)arg1).size());
	}
	
	else if(arg0 instanceof Elevator && arg1 instanceof ElevatorState){
		Elevator e = ((Elevator) arg0);
		int y = e.getID();
		int x = e.getCurrentFloor();
		
		System.out.println("dsadada");

		if(arg1 instanceof Move){
			this.setClosed(x,y);
			System.out.println("VIEW MOVE");
			
		}
		
				
		else if(arg1 instanceof Door){
			if(e.isDoorOpen()){
				this.clearColumn(y);
				this.setOpened(x, y);
				
			}
			else if(!e.isDoorOpening()){
				this.clearColumn(y);
				this.setClosing(x, y);
			}
			
			else if(e.isDoorOpening()){
				this.clearColumn(y);
				this.setOpening(x, y);
			}
			
		}
		
		else if(arg1 instanceof Ready){
			this.clearColumn(y);
			this.setReady(x, y);
		}
		
		
		
	}
	
	else if(arg0 instanceof Elevator && arg1 instanceof Integer){
		Elevator e = ((Elevator) arg0);
		this.setFloorLamp(e.getID(), (Integer) arg1);
		this.clearColumn(e.getID());
		this.setClosed(e.getCurrentFloor(),e.getID());
	}
	
	
	
}
 
  
  


}
