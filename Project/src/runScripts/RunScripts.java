package runScripts;

import java.io.IOException;

public class RunScripts {
	
	public static void runScripts(String scriptName){
		try {
			Runtime.getRuntime().exec("cmd.exe /c start " + scriptName);
		} catch (IOException e) {
			System.out.println("Cannot open file");
		}
	}

}
