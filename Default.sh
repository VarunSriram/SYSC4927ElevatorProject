#!/bin/bash

echo -n "Enter the maximum number of Patrons [integer]: "
read maxPatron

echo -n "Enter number of floors in the building [integer]: "
read numberOfFloors

echo -n "Enter number of elevators in the building [integer]: "
read numberOfElevators

echo -n "Enter the distance between each floor in the building [float]: "
read floorDistance

echo -n "Enter the frequency of the Monitor polling for data [float]: "
read pollTime

echo -n "Enter the speed of the elevators in the building [float]: "
read elevatorSpeed

echo -n "Enter the speed of the elevator doors opening and closing [integer]: "
read doorSpeed

echo -n "Enter the maximum capacity of the elevators [integer]: "
read elevatorCapacity

echo -n "Enter the dispatching strategy to use (Sectors, NearestElevator, Automatic) [string]: "
read strategy

javac -cp /usr/share/java/junit4.jar Project/src/*/*.java 

java -cp Project/bin/ main.Main $maxPatron $numberOfFloors $numberOfElevators $floorDistance $pollTime $elevatorSpeed $doorSpeed $elevatorCapacity $strategy
