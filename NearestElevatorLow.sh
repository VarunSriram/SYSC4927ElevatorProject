!#bin/bash
maxPatron=10
numberOfFloors=12
numberOfElevators=2
floorDistance=10.5
pollTime=3.0
elevatorSpeed=5.0
doorSpeed=1000
elevatorCapacity=15000
strategy=NearestElevator
javac -cp /usr/share/java/junit4.jar Project/src/*/*.java
java -cp Project/bin/ main.Main $maxPatron $numberOfFloors $numberOfElevators $floorDistance $pollTime $elevatorSpeed $doorSpeed $elevatorCapacity $strategy